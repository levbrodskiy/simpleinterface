﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class PrinterJSON : MonoBehaviour
{
    public InputField InputName;
    public InputField InputSurName;
    public InputField InputMiddleName;
    public InputField InputPhone;
    public InputField InputEmail;
    public InputField InputAddress;
    public Button sendButton;
    
    private string name;
    private string surName;
    private string middleName;
    private string phone;
    private string email;
    private string address;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void Print()
    {
        name = InputName.text;
        surName = InputSurName.text;
        middleName = InputMiddleName.text;
        phone = InputPhone.text;
        email = InputEmail.text;
        address = InputAddress.text;

        if(validation()){
            string json = JsonUtility.ToJson(
                new InputData(name, surName, middleName, phone, email, address), true);
            print(json);
        }else{
            print("invalid data!");
        }
    }

    private bool validation(){
       string emailRegex = @"\A(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
       string nameRegex = @"^[А-Я]?[а-я]{1,21}$";
       string phoneRegex = @"^[+]{0,1}[0-9]{11}$";
       string addressRegex = @"^[А-Яа-я\s\.0-9]{4,50}$";
       
       if( !( Regex.IsMatch(name, nameRegex) && 
       Regex.IsMatch(surName, nameRegex) && 
       Regex.IsMatch(middleName, nameRegex) &&
       Regex.IsMatch(email, emailRegex) &&
       Regex.IsMatch(phone, phoneRegex) &&
       Regex.IsMatch(address, addressRegex) ) ){
           return false;
       }
         return true;
    }

    private class InputData
    {
        public string name;
        public string surName;
        public string middleName;
        public string phone;
        public string email;
        public string address;
        public InputData(string name, string surname, string middleName,
        string phone, string email, string address)
        {
            this.name = name;
            this.surName = surName;
            this.middleName = middleName;
            this.phone = phone;
            this.email = email;
            this.address = address;
        }
    }
}